var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var http = require("http").Server(app);
var port = process.env.PORT || 8017;
var uuid = require('node-uuid');
var _ = require('lodash');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static("dist"));
app.use(bodyParser.json());

/*** SECURITY ***/
// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Pass to next layer of middleware
    next();
});

http.listen(port, function () {
    console.log("Server started.  Listening on *:" + port);
});

app.get("/", function (req, res) {
    res.send({
        "status": "OK"
    })
})

/*** SERVICES ***/
var LocationService = require("./src/services/location.js");
var locationService = new LocationService("seattle");

var DBService = require("./src/services/db.js");
var dbService = new DBService();

var MockDataGenerator = require("./src/services/mock-data-generator.js");
var mockDataGenerator = new MockDataGenerator();

/*** BRIDGES ***/
/*** city grid ***/
var CityGridBridge = require("./src/bridges/city-grid.js");
var cityGridBridge = new CityGridBridge();

app.get("/api/restaurants", function (req, res) {
    // search locations
    cityGridBridge.searchLocations({
        category: "restaurant",
        latitude: locationService.location.latitude,
        longitude: locationService.location.longitude,
        numberOfPages: 5,
        radius: 2,
        resultsPerPage: 20
    }, res);
})
/*** city grid ***/
/*** google maps ***/
var GoogleMapsBridge = require("./src/bridges/google-maps.js");
var googleMapsBridge = new GoogleMapsBridge();
app.get("/api/typeahead/cities", function (req, res) {
    let sCb = (data, mapper) => {
        data = data ? data.map(mapper) : null;
        res.send({
            "status": "OK",
            "data": data
        });
    }
    googleMapsBridge.searchCitiesByName({
        name: req.query.name
    }, sCb)
})

app.get("/api/places", function (req, res) {
    let sCbSearchPlaces = (data, mapper) => {
        data = data ? data.map(mapper) : null;
        res.send({
            "status": "OK",
            "data": data
        });
    }
    let sCbFindLocation = (data, mapper) => {
        data = data ? data.map(mapper) : null;
        let datum = data && _.size(data) > 0 ? data[0] : null;
        if (datum) {
            // Find places by location
            googleMapsBridge.searchPlacesByLocation({
                location: `${datum.latitude},${datum.longitude}`,
                query: req.query.criteria
            }, sCbSearchPlaces)
        } else {
            res.send({
                "status": "BAD_REQUEST"
            })
        }
    }
    // Find the general lat, lng
    googleMapsBridge.findLocationByText({
        query: req.query.query
    }, sCbFindLocation);
})
/*** google maps ***/

/*** mock data generator ***/
app.get("/api/companies/:num(\\d+)/", function (req, res) {
    let companies = [];
    for (let i = 0; i < req.params.num; i++) {
        companies.push({
            name: mockDataGenerator.activityName(),
            address: mockDataGenerator.activityAddress(),
            image: mockDataGenerator.activityImage(192, 192),
            price: mockDataGenerator.activityPrice(),
            time: mockDataGenerator.activityDateTime()
        })
    }
    res.send(companies);
})
/*** mock data generator ***/

/*** dynamoDB ***/
app.post("/api/contributions/create", function (req, res) {

    /*
     let params = {
            author: {
                firstName: contributionFormData.author.firstName,
                lastName: contributionFormData.author.lastName,
                age: contributionFormData.author.age,
                email: contributionFormData.author.email
            },
            contribution: {
                title: this.state.typeAheadSelection ? this.state.typeAheadSelection : this.contribution.title.value,
                description: this.getStringDescription(),
                timing: this.contribution.timing.value,
                type: contributionFormData.type,
                secondaryType: contributionFormData.type === "Other" ? startCase(this.contribution.type.value) : null
            }
        };
        */
    var {
        author: {
            firstName,
            lastName,
            age,
            email
        },
        contribution: {
            title,
            description,
            type,
            secondaryType,
            timing
        }
    } = req.body;
    var params = {
        TableName: "Contributions",
        Item: {
            "Title": title,
            "AuthorName": firstName + " " + lastName,
            "AuthorFirstName": firstName,
            "AuthorLastName": lastName,
            "AuthorAge": age,
            "AuthorEmail": email,
            "Description": description,
            "Timing": timing,
            "Category": type,
            "SecondaryCategory": secondaryType,
            "LastModified": Date.now(),
            "ID": uuid.v4()
        }
    }
    // create Contributions
    dbService.createItem(params, (data) => {
        console.log("Contribution has been created")
        res.send();
    })
})

app.post("/api/contributions", function (req, res) {
    var {
        types,
        limit
    } = req.body;

    if (!types) {
        res.send({
            msg: "Invalid types were sent"
        })
    }
    let promises = [];
    types.forEach(type => {
        var params = {
            TableName: 'Contributions',
            KeyConditionExpression: 'Category = :category',
            ExpressionAttributeValues: {
                ':category': type
            },
            ScanIndexForward: true,
            ConsistentRead: false,
            Select: 'ALL_ATTRIBUTES',
            Limit: limit || 10
        };
        promises.push(new Promise((resolve, reject) => {
            // read Contributions by type
            dbService.query(params, (data) => {
                console.log("Contributions have been retrieved")
                resolve(data ? data.Items : []);
            })
        }))
    });
    Promise.all(promises).then(results => {
        let data = [];
        if (results) {
            results.forEach(result => {
                if (result) {
                    result.forEach(r => {
                        //FIXME: temporarily returning some images url
                        r.ImageUrl = mockDataGenerator.activityImage(192, 192);
                        data.push(r);
                    })
                }
            })
        }
        res.send(data);
    })
})
/*** dynamoDB ***/