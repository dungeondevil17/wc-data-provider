var faker = require("faker");

function MockDataGenerator() {
    this.personName = () => {
        return faker.name.firstName() + faker.name.lastName();
    }
    this.activityImage = (width, height) => {
        return `https://loremflickr.com/${width}/${height}/${faker.address.state()}`;
    }
    this.activityDateTime = () => {
        return faker.date.past();
    }
    this.activityAddress = () => {
        return {
            city: faker.address.city(),
            country: faker.address.country(),
            streetAddress: faker.address.streetAddress(),
            state: faker.address.state()
        }
    }
    this.activityPrice = () => {
        return faker.commerce.price();
    }
    this.activityName = () => {
        return faker.company.companyName();
    }
}

module.exports = MockDataGenerator;