var where = require("node-where");
var ip = require("ip");

var PreDefinedLocations = require("../data/pre-defined-locations.js");
var preDefinedLocations = new PreDefinedLocations();

function Location(key) {
    this.location = {
        latitude: undefined,
        longitude: undefined
    }

    this.getLocation = function(key) {
        // get my location
        this.location.latitude = preDefinedLocations.coordinates[key].latitude;
        this.location.longitude = preDefinedLocations.coordinates[key].longitude;
    }

    this.getLocation(key);
}

module.exports = Location;