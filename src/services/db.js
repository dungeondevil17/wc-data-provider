var AWS = require('aws-sdk');
const challenges = require("../../dynamodb/tables/contributions.js");
const tables = [challenges];

AWS.config.loadFromPath('./dynamodb/config/creds-remote.json');


/****RESOURCES****/
// https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SQLtoNoSQL.ReadData.Query.html
/*****************/

function DB() {
    // init the database
    var dynamoDb = new AWS.DynamoDB();

    const documentClient = new AWS.DynamoDB.DocumentClient();

    // create the tables
    if (tables) {
        tables.forEach((table) => {
            dynamoDb.createTable(table, (err, data) => {
                if (err) {
                    console.error("Could not create table ", table, err);
                } else {
                    console.log("Table created successfully ", data);
                }
            })
        })
    }

    this.createItem = (params, cb) => {
        // create item
        documentClient.put(params, (err, data) => {
            if (err) {
                console.error("Could not put the item", err);
            } else {
                if (cb) {
                    cb(data);
                }
            }
        })
    };

    this.query = (params, cb) => {
        // query 
        documentClient.query(params, (err, data) => {
            if (err) {
                console.error("Could not put the item", err);
            } else {
                if (cb) {
                    cb(data);
                }
            }
        })
    };

    this.getItem = (params, cb) => {
        // get item
        documentClient.get(params, (err, data) => {
            if (err) {
                console.error("Could not get the item", err);
            } else {
                if (cb) {
                    cb(data);
                }
            }
        })
    };
}

module.exports = DB;