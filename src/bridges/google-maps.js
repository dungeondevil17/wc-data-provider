var axios = require("axios");
var _ = require("lodash");
var moment = require("moment");
var uuid = require('node-uuid');

//https://developers.google.com/places/web-service/autocomplete#place_types
const API_SEARCH_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
const API_SEARCH_PLACE_TEXT = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json";
const API_SEARCH_TEXT = "https://maps.googleapis.com/maps/api/place/textsearch/json";
const API_PLACE_DETAIL = "https://maps.googleapis.com/maps/api/place/details/json";

var API_KEY = "AIzaSyC68-eL6orlUPgCIrWdRoOm_bvhEekLBKo";

function GoogleMapsBridge() {
    this.searchCitiesByName = function (entity, sCb) {
        /**
         * entity = {
         *  name
         * }
         */

        this.executeQuery(API_SEARCH_AUTOCOMPLETE, {
            input: entity.name,
            types: "(cities)",
            key: API_KEY
        }).then((response) => {
            // parse the response
            let mapper = (datum) => {
                return {
                    name: datum.description
                }
            }
            sCb(response.data.predictions, mapper);
        }).catch((err) => {
            console.error(err);
            this.responseCallback(res, "BAD REQUEST", err);
        });
    }

    this.searchPlacesByLocation = function (entity, sCb) {
        /**
         * entity = {
         *  location
         *  query
         * }
         */
        this.executeQuery(API_SEARCH_TEXT, {
            location: entity.location,
            query: entity.query,
            key: API_KEY
        }).then((response) => {
            let results = response.data.results;
            let promises = [];
            results.forEach(result => {
                let {
                    place_id: placeid
                } = result;
                promises.push(new Promise((resolve, reject) => {
                    this.getDetailsByPlace({
                        placeid
                    }, resolve)
                }));
            });
            // parse the response
            let mapper = (datum) => {
                let {
                    place_id
                } = datum;
                datum.external_id = place_id;
                datum.external_source = "google_place_search";
                return datum;
            }
            Promise.all(promises).then(data => sCb(data, mapper));
        }).catch((err) => {
            console.error(err);
            this.responseCallback(res, "BAD REQUEST", err);
        });
    }

    this.findLocationByText = function (entity, cb) {
        /**
         * entity = {
         *  query
         * }
         */
        this.executeQuery(API_SEARCH_PLACE_TEXT, {
            input: entity.query,
            inputtype: "textquery",
            fields: "geometry",
            key: API_KEY
        }).then((response) => {
            // parse the response
            let mapper = (datum) => {
                let {
                    geometry
                } = datum;
                return {
                    latitude: geometry.location.lat,
                    longitude: geometry.location.lng
                }
            }
            cb(response.data.candidates, mapper);
        }).catch((err) => {
            console.error(err);
            this.responseCallback(res, "BAD REQUEST", err);
        });
    }

    this.getDetailsByPlace = function (entity, cb) {
        /**
         * entity = {
         *  placeid
         * }
         */
        this.executeQuery(API_PLACE_DETAIL, {
            placeid: entity.placeid,
            key: API_KEY
        }).then((response) => {
            // parse the response
            let mapper = (datum) => {
                let {
                    formatted_address,
                    formatted_phone_number,
                    name,
                    opening_hours,
                    weekday_text,
                    photos,
                    price_level,
                    rating,
                    reviews,
                    website
                } = datum;
                let hours = [];
                let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                let periods = opening_hours && opening_hours.periods;
                if (periods) {
                    periods.forEach(period => {
                        hours.push({
                            open: {
                                day: days[period.open.day],
                                time: moment(period.open.time, "HH:mm").format("hh:mm a")
                            },
                            close: {
                                day: days[period.close.day],
                                time: moment(period.close.time, "HH:mm").format("hh:mm a")
                            }
                        })
                    });
                }
                return {
                    id: uuid.v4(),
                    formatted_address,
                    formatted_phone_number,
                    name,
                    hours,
                    formatted_hours: weekday_text,
                    open_now: opening_hours ? opening_hours.open_now : null,
                    photos,
                    price_level,
                    rating,
                    reviews,
                    website
                };
            }
            cb(mapper(response.data.result));
        }).catch((err) => {
            console.error(err);
            this.responseCallback(res, "BAD REQUEST", err);
        });
    }

    this.executeQuery = function (uri, params) {
        return new Promise((resolve, reject) => {
            axios.get(uri, {
                params
            }).then((response) => {
                resolve(response);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    this.responseCallback = function (res, status, data, mapper) {
        //todo: move this outta here + clean this up
        data = data ? data.map(mapper) : null;
        res.send({
            "status": "OK",
            "data": data
        });
    }
}

module.exports = GoogleMapsBridge;