var axios = require("axios");
var _ = require("lodash");
var MockDataGenerator = require("../services/mock-data-generator.js");
var mockDataGenerator = new MockDataGenerator();

const API_SEARCH_WHERE = "https://api.citygridmedia.com/content/places/v2/search/where";
const API_SEARCH_LATLON = "https://api.citygridmedia.com/content/places/v2/search/latlon";

const PUBLISHER = "10000023218";
const API_RESPONSE_FORMAT = "json";

function CityGridBridge() {
    this.searchLocations = function (entity, res) {
        /**
         * entity = {
         *  category, 
         *  subject, 
         *  startsWithCharacter,
         *  where,
         *  resultsPerPage,
         *  sort,
         *  latitude,
         *  longitude,
         *  radius,
         *  numberOfPages
         * }
         */

        // search based on latitude and longitude
        if (entity.latitude && entity.longitude) {
            this.searchLocationOnAllPages(API_SEARCH_LATLON, {
                type: entity.category,
                what: entity.subject,
                first: entity.startWithCharacter,
                rpp: entity.resultsPerPage,
                sort: entity.sort,
                publisher: PUBLISHER,
                format: API_RESPONSE_FORMAT,
                lat: entity.latitude,
                lon: entity.longitude,
                radius: entity.radius
            }, entity.numberOfPages).then((response) => {
                // parse the response
                this.responseCallback(res, "OK", response);
            }).catch((err) => {
                console.error(err);
                this.responseCallback(res, "BAD REQUEST", err);
            });
        }
        // search based on free form location 
        else {
            this.searchLocationOnAllPages(API_SEARCH_WHERE, {
                type: entity.category,
                what: entity.subject,
                where: entity.where,
                first: entity.startWithCharacter,
                rpp: entity.resultsPerPage,
                sort: entity.sort,
                publisher: PUBLISHER,
                format: API_RESPONSE_FORMAT
            }, entity.numberOfPages).then((response) => {
                // parse the response
                this.responseCallback(res, "OK", response);
            }).catch((err) => {
                console.error(err);
                this.responseCallback(res, "BAD REQUEST", err);
            });
        }
    }

    this.searchLocationOnPage = function (uri, params, page) {
        return new Promise((resolve, reject) => {
            axios.get(uri, {
                params: Object.assign(params, {
                    page: page
                })
            }).then((response) => {
                resolve(response.data.results);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    this.searchLocationOnAllPages = function (uri, params, numberOfPages) {
        var promises = [];
        promises.push(this.searchLocationOnPage(uri, params, 1));
        return Promise.all(promises);
    }

    this.responseCallback = function (res, status, data) {
        //todo: move this outta here + clean this up
        data = data && data[0] ? data[0].locations : null;
        data = data ? data.map((datum) => {
            return {
                name: datum.name,
                address: datum.address,
                image: datum.image || mockDataGenerator.activityImage(384, 384),
                price: null,
                time: null
            }
        }) : null;
        res.send({
            "status": "OK",
            "data": data
        });
    }
}

module.exports = CityGridBridge;