function PreDefinedLocations() {
    this.coordinates = {
        "harlem": {
            latitude: "40.811550",
            longitude: "-73.946477"
        },
        "new orleans": {
            latitude: "29.951066",
            longitude: "-90.071532"
        },
        "seattle": {
            latitude: "47.606209",
            longitude: "-122.332071"
        }
    }
}

module.exports = PreDefinedLocations;