var params = {
    TableName: 'Contributions',
    KeyConditionExpression: 'Category = :category', // a string representing a constraint on the attribute
    ExpressionAttributeValues: { // a map of substitutions for all attribute values
        ':category': 'Tip'
    },
    ScanIndexForward: true, // optional (true | false) defines direction of Query in the index
    ConsistentRead: false, // optional (true | false)
    Select: 'ALL_ATTRIBUTES', // optional (ALL_ATTRIBUTES | ALL_PROJECTED_ATTRIBUTES | 
    Limit: 10 //           SPECIFIC_ATTRIBUTES | COUNT)
};
docClient.query(params, function (err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
///http://localhost:8000/shell/