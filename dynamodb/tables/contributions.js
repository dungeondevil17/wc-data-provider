var challenges = {
    TableName: "Contributions",
    KeySchema: [{
        AttributeName: "Category",
        KeyType: "HASH"
    }, {
        AttributeName: "LastModified",
        KeyType: "RANGE"
    }],
    AttributeDefinitions: [{
        AttributeName: "Category",
        AttributeType: "S"
    }, {
        AttributeName: "LastModified",
        AttributeType: "N"
    }],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
    }
}

module.exports = challenges;